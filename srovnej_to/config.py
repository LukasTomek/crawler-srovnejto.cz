
## Internet

internet = 1 #1 for include, 0 for exclude
internet_params = 'Základní' #Základní for basic, Rozšířené for extended
 
 ### Internet basics

no_people = '2' #'1', '2' or '3 a vice'
no_devices = '3-4' #'1-2', '3-4' or '5 a vice'
download_speed = 150 #Mb/s from 2 to 1_000

internet_usage_basic = dict([
    (1 , 1), #web brosing
    (2 , 1), #mail
    (3 , 1), #socials
    (4 , 1), #videos
    (5 , 0), #music
    (6 , 0), #home office
    (7 , 0), #smart home
    (8 , 1), #gaming
])

internet_usage_extended = dict([
    (1 , 0), #XDSL
    (2 , 0), #Optic
    (3 , 1), #mobile connection
    (4 , 0), #wireless connection
    (5 , 1), #plug-in connection
])

