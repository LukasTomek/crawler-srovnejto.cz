import numpy as np
import pandas as pd
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
import time
from srovnej_to import config
from srovnej_to import crawler

if __name__ == '__main__':
    worker = crawler.crawler()
    evaluate = crawler.evaluation()
    if config.internet == 1:
        worker.get_internet_offers()
        evaluate.evaluate_internet_offers(method = 'price')

