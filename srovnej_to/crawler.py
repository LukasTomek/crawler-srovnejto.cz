import numpy as np
import pandas as pd
from tabulate import tabulate
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
import time
from srovnej_to import config

class crawler(object):

    def __init__(self):
        self.browser = webdriver.Firefox(executable_path='./geckodriver')
        self.browser.get('https://www.srovnejto.cz/internet/kalkulace/')
        self.wait = WebDriverWait(self.browser,20)
    
    def get_internet_offers(self):
        # wait for the page to fully load
        clickable_element = expected_conditions.element_to_be_clickable(
            (By.XPATH, '/html/body/div[1]/div[2]/div/div/div[2]/button[2]')
            )
        self.wait.until(clickable_element)

        # specify fields on internet page
        params_button = '//span[text()=\'' + str(config.internet_params) + '\']'
        people_button = '//span[text()=\'' + str(config.no_people) + '\']'
        device_button = '//span[text()=\'' + str(config.no_devices) + '\']'
        down_speed_button = self.browser.find_element(By.XPATH, '//input[@name=\'downloadSpeed\']')
        submit_button = self.browser.find_element(By.XPATH, '//*[@type=\'submit\']')

        # find and click on cookies button
        cookies_button = self.browser.find_element_by_xpath(
            '/html/body/div[1]/div[2]/div/div/div[2]/button[2]'
            )
        cookies_button.click()

        # get internet prices by inserting basic specification
        if str(config.internet_params) == 'Základní': 
            # find parameter button for basic specification and click on it
            clickable_element = expected_conditions.element_to_be_clickable((By.XPATH, params_button))
            self.wait.until(clickable_element)
            parameters = self.browser.find_element_by_xpath(params_button)
            parameters.click()

            time.sleep(2)

            # click on number of people in a household
            device_button.click()
            time.sleep(1)

            # click on number of connected devices
            people_button.click()
            time.sleep(1)

            
            # loop over checkboxes
            for i in range(1,9,1):
                clickable_element = expected_conditions.element_to_be_clickable(
                (By.XPATH, '//*[text()="Využití internetu"]/../div[1]/label[' + str(i) + ']/span')
                )
                self.wait.until(clickable_element)
                internet_usage = self.browser.find_element(
                    by=By.XPATH, value='//*[text()="Využití internetu"]/../div[1]/label[' + str(i) + ']/input[1]'
                    )
                if internet_usage.get_property('checked') == False and config.internet_usage_basic[i] == 1:
                    self.browser.find_element(
                        by=By.XPATH, value='//*[text()="Využití internetu"]/../div[1]/label[' + str(i) + ']/span'
                        ).click()
                if internet_usage.get_property('checked') == True and config.internet_usage_basic[i] == 0:
                    self.browser.find_element(
                        by=By.XPATH, value='//*[text()="Využití internetu"]/../div[1]/label[' + str(i) + ']/span'
                        ).click()

        # get internet prices by inserting advanced specification
        if str(config.internet_params) == 'Rozšířené':
            # find parameter button for extended specification and click on it
            clickable_element = expected_conditions.element_to_be_clickable((By.XPATH, params_button))
            self.wait.until(clickable_element)
            parameters = self.browser.find_element_by_xpath(params_button)
            parameters.click()

            # insert download speed
            down_speed_button.send_keys(config.download_speed)

            #loop over checkboxes
            for i in range(1,5,1):
                clickable_element = expected_conditions.element_to_be_clickable(
                (By.XPATH, '//*[text()="Typ internetového připojení"]/../div[1]/label[' + str(i) + ']/span')
                )
                self.wait.until(clickable_element)
                internet_usage = self.browser.find_element(
                    by=By.XPATH, value='//*[text()="Typ internetového připojení"]/../div[1]/label[' + str(i) + ']/input[1]'
                    )
                if internet_usage.get_property('//*checked') == False and config.internet_usage_extended[i] == 1:
                    self.browser.find_element(
                        by=By.XPATH, value='//*[text()="Typ internetového připojení"]/../div[1]/label[' + str(i) + ']/span'
                        ).click()
                if internet_usage.get_property('checked') == True and config.internet_usage_extended[i] == 0:
                    self.browser.find_element(
                        by=By.XPATH, value='//*[text()="Typ internetového připojení"]/../div[1]/label[' + str(i) + ']/span'
                        ).click()
        
        #click on submit button
        time.sleep(1)
        submit_button.click()
        internet_results = pd.DataFrame(columns=['Provider','Speed','FUP','Price'])

        self.browser.find_element_by_xpath(
            '//*[@id="app"]/div/div/main/div/div[3]/div[1]/div/div/div[4]/div[2]/div[1]/div[2]/div[1]/label/select/option[2]'
            ).click()
        i = 0
        while True:
            i = i+1
            try:
                row = [self.browser.find_element(
                            by=By.XPATH, value='//*[@class=\'offers-list-header\']/../div[2]/div[' 
                                + str(i) + 
                                ']/div[2]/div[1]/div[1]/h3'
                            ).get_attribute('innerHTML'),

                        self.browser.find_element(
                            by=By.XPATH, value='//*[@class=\'offers-list-header\']/../div[2]/div[' 
                                + str(i) + 
                                ']/div[2]/div[1]/div[2]/p'
                            ).get_attribute('innerHTML'),

                        self.browser.find_element(
                            by=By.XPATH, value='//*[@class=\'offers-list-header\']/../div[2]/div[' 
                                + str(i) + 
                                ']/div[2]/div[1]/div[3]/span'
                            ).get_attribute('innerHTML'),

                        self.browser.find_element(
                            by=By.XPATH, value='//*[@class=\'offers-list-header\']/../div[2]/div[' 
                                + str(i) + 
                                ']/div[2]/div[1]/div[4]/h3'
                            ).get_attribute('innerHTML') ]
                internet_results.loc[i-1] = row
            except:
                try:
                    row = [self.browser.find_element(
                            by=By.XPATH, value='//*[@class=\'offers-list-header\']/../div[2]/div[' 
                                + str(i) + 
                                ']/div[3]/div/div[1]/h3'
                            ).get_attribute('innerHTML'),
                        self.browser.find_element(
                            by=By.XPATH, value='//*[@class=\'offers-list-header\']/../div[2]/div[' 
                                + str(i) + 
                                ']/div[3]/div/div[2]/p'
                            ).get_attribute('innerHTML'),
                        self.browser.find_element(
                            by=By.XPATH, value='//*[@class=\'offers-list-header\']/../div[2]/div[' 
                                + str(i) + 
                                ']/div[3]/div/div[3]/span'
                            ).get_attribute('innerHTML'),
                        self.browser.find_element(
                            by=By.XPATH, value='//*[@class=\'offers-list-header\']/../div[2]/div[' 
                                + str(i) + 
                                ']/div[3]/div/div[4]/h3'
                            ).get_attribute('innerHTML') ]
                    internet_results.loc[i-1] = row
                except:
                    break
        print(internet_results.transpose())
        internet_results.to_csv('./outputs/interner_prices.csv')

class evaluation(object):

    def __init__(self):
        self.data = pd.read_csv('./outputs/interner_prices.csv')

    def evaluate_internet_offers(self, method = 'price'):
        for i in range(len(self.data['Speed'])):
            self.data.loc[i, 'Down_Speed'] = [int(n) for n in self.data['Speed'][i].split() if n.isdigit()][0]
            self.data.loc[i, 'Up_Speed'] = [int(n) for n in self.data['Speed'][i].split() if n.isdigit()][1]
            self.data.loc[i, 'Price_CZK'] = [int(n) for n in self.data['Price'][i].split() if n.isdigit()][0]

        data_min_price = self.data[self.data['Price_CZK']==min(self.data['Price_CZK'])]
        data_max_down = self.data[self.data['Down_Speed']==max(self.data['Down_Speed'])]
        data_max_up = self.data[self.data['Up_Speed']==max(self.data['Up_Speed'])]

        if method == 'price':
            print('Cheapest offers for ' + str(min(self.data['Price_CZK'])) + ' CZK are following:')
            print(tabulate(data_min_price[['Provider','Speed','FUP','Price_CZK']],
                showindex='never', tablefmt='fancy_grid', headers='keys'))

        elif method == 'down':
            print('Offers with fastest download speed of ' + str(max(self.data['Down_Speed'])) + ' Mb/s are following:')
            print(tabulate(data_max_down[['Provider','Speed','FUP','Price_CZK']],
                showindex='never', tablefmt='fancy_grid', headers='keys'))

        elif method == 'up':
            print('Offers with fastest upload speed of ' + str(max(self.data['Up_Speed'])) + ' Mb/s are following:')
            print(tabulate(data_max_up[['Provider','Speed','FUP','Price_CZK']],
                showindex='never', tablefmt='fancy_grid', headers='keys'))

        else:
            print(
                'Wrong method in evaluate internet offers, possible options are : "price" for cheapest offers, "down" for offers with maximal download speed and "up" for offers with maximal upload speed.')
                    